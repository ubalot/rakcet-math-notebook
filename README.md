# Racket project boilerplate

Run the app
```bash
racket main.rkt
```

Run the tests
```bash
raco test tests/
```
