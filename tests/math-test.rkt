#lang racket

(require rackunit)
(require "./utils.rkt")
(require "../lib/math.rkt")

(define divisors-test-suites
  (test-suite
    "Divisors test suite"

    (test-suite
      "Number divisors"

      (test-equal? "divisors of 30" (divisors 30) (list 1 2 3 5 6 10 15 30))
      (test-equal? "divisors of -30" (divisors -30) (list -1 -2 -3 -5 -6 -10 -15 -30))

      (test-equal? "divisors of 0" (divisors 0) (list))

      (test-equal? "divisors of 1" (divisors 1) (list 1))
      (test-equal? "divisors of -1" (divisors -1) (list -1)))

    (test-suite
      "Great common divisors"

      (test-equal? "gcd(4, 12)" (Euclid-gcd 4 12) 4))))

(define primes-test-suites
  (test-suite
    "Various prime numbers types"

    (test-suite
      "Prime numbers"

      (test-true "is 3 prime?" (prime? 3))
      (test-false "is 4 prime?" (prime? 4))

      (test-equal? "primes below 20" (primes 20) (list 1 2 3 5 7 11 13 17 19)))

    (test-suite
      "Marsenne prime numbers"

      (test-equal?
        "Marsenne-primes below 20"
        (Marsenne-primes 20)
        (list 3 7 31 127 2047 8191 131071 524287))

      (test-equal?
        "Marsenne-primes-couples below 20"
        (Marsenne-primes-couples 20)
        (list (cons 1 1)
              (cons 3 6)
              (cons 7 28)
              (cons 31 496)
              (cons 127 8128)
              (cons 2047 2096128)
              (cons 8191 33550336)
              (cons 131071 8589869056)
              (cons 524287 137438691328))))

    (test-suite
      "Crazy prime numbers"

      (test-true "is 5 a crazy-prime?" (crazy-prime? 5)))))

(define special-numbers-test-suites
  (test-suite
    "Perfect numbers"

    (test-true "is 6 a perfect-number?" (perfect-number? 6))))

(define geometry-test-suites
  (test-suite
    "Geometry formulas"

    (test-suite
      "Pythagorean triples"

      (test-equal?
        "pythagorean-triples below 20"
        (pythagorean-triples 20)
        (list (list 3 4 5)
              (list 8 6 10)
              (list 5 12 13)
              (list 15 8 17)
              (list 12 16 20))))

    (test-suite
      "Heron's triangle area formula"

      (test-equal? "Triangle area" (Heron-triangle-area 3 4 5) 6))))

(define algebraic-test-suites
  (test-suite
    "Algebric formulas"

    (test-suite
      "Pell equations"

      (test-equal?
        "Pell-equations-triples(3, 10)"
        (Pell-equations-triples 3 10)
        (list (list 2 1 1)
              (list 7 4 1))))))

(exit-with-failure-or-error divisors-test-suites)
(exit-with-failure-or-error primes-test-suites)
(exit-with-failure-or-error special-numbers-test-suites)
(exit-with-failure-or-error geometry-test-suites)
(exit-with-failure-or-error algebraic-test-suites)
