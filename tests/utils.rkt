#lang racket

(provide exit-with-failure-or-error)

(require rackunit)

(define (exit-with-failure-or-error test)
  (fold-test-results
    (lambda (result seed)
      (unless (test-success? result)
        (displayln (format "Test failed: ~a" (current-test-name)))
        (exit 1)))
    null
    test))